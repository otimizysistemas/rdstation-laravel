<?php

namespace Otimizy\RdStation\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Middleware;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use Otimizy\RdStation\RdStationAdapter;

class RdStationAdapterTest extends TestCase
{
    protected $config;
    protected $container = [];

    public function setUp(): void
    {
        parent::setUp();

        $this->config = [
            'client_id' => '123456789',
            'client_secret' => '987654321',
            'redirect_url' => 'https://mysite.com/callback',
        ];
    }

    public function testAuthUrl()
    {
        $adapter = $this->createAdapter(new Response);

        $url = $adapter->authUrl();

        $exptectedUrl = 'https://api.rd.services/auth/dialog?client_id='
            . $this->config['client_id'] . '&redirect_url='
            . urlencode($this->config['redirect_url']);

        $this->assertEquals($exptectedUrl, $url);
        $this->assertCount(0, $this->container);
    }

    public function testGetToken()
    {
        $body = [
            'access_token' => '123456789',
            'expires_in' => 10,
            'refresh_token' => '987654321',
        ];

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->getToken('123456');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('auth/token', $request->getUri()->getPath());
    }

    public function testGetTokenError()
    {
        $body = [ 'error' => 'test error' ];

        $adapter = $this->createAdapter(new Response(400, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->getToken('123456');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 400, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('auth/token', $request->getUri()->getPath());
    }

    public function testRefreshToken()
    {
        $body = [
            'access_token' => '123456789',
            'expires_in' => 10,
            'refresh_token' => '987654321',
        ];

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->refreshToken('456123789');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('auth/token', $request->getUri()->getPath());
    }

    public function testRefreshTokenError()
    {
        $body = [ 'error' => 'test error' ];

        $adapter = $this->createAdapter(new Response(400, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->refreshToken('456123789');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 400, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('auth/token', $request->getUri()->getPath());
    }

    public function testCreateLead()
    {
        $body = [ 'event_uuid' => '111222333' ];

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->createLead('teste', 'teste@otimizy.com.br', [
            'name' => 'Envio teste',
            'personal_phone' => '(54) 9999-99999',
            'traffic_source' => 'fb',
            'traffic_medium' => 'cpc',
            'traffic_campaign' => 'campanha',
        ], '333222111');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('platform/events', $request->getUri()->getPath());
    }

    public function testCreateLeadError()
    {
        $body = [ 'error' => 'test error' ];

        $adapter = $this->createAdapter(new Response(400, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->createLead('teste', 'teste@otimizy.com.br', [
            'name' => 'Envio teste',
            'personal_phone' => '(54) 9999-99999',
            'traffic_source' => 'fb',
            'traffic_medium' => 'cpc',
            'traffic_campaign' => 'campanha',
        ], '333222111');

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 400, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('platform/events', $request->getUri()->getPath());
    }

    public function testSetOpportunity()
    {
        $body = [ 'event_uuid' => '111222333' ];

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->setOpportunity(
            'teste@otimizy.com.br',
            'teste',
            '333222111'
        );

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('platform/events', $request->getUri()->getPath());
    }

    public function testSetOpportunityError()
    {
        $body = [ 'error' => 'test error' ];

        $adapter = $this->createAdapter(new Response(400, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->setOpportunity(
            'teste@otimizy.com.br',
            'teste',
            '333222111'
        );

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 400, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('platform/events', $request->getUri()->getPath());
    }

    public function testSetWon()
    {
        $body = [ 'event_uuid' => '111222333' ];

        $adapter = $this->createAdapter(new Response(200, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->setWon(
            'teste@otimizy.com.br',
            'teste',
            '333222111'
        );

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 200, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('platform/events', $request->getUri()->getPath());
    }

    public function testSetWonError()
    {
        $body = [ 'error' => 'test error' ];

        $adapter = $this->createAdapter(new Response(400, [
            'Content-Type' => 'application/json',
        ], json_encode($body)));

        $response = $adapter->setWon(
            'teste@otimizy.com.br',
            'teste',
            '333222111'
        );

        $this->assertIsArray($response);
        $this->assertEquals([ 'status' => 400, 'body' => $body ], $response);
        $this->assertCount(1, $this->container);

        $request = $this->container[0]['request'];

        $this->assertInstanceOf(Request::class, $request);
        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals('platform/events', $request->getUri()->getPath());
    }

    protected function createAdapter(Response $response)
    {
        $client = $this->createMockClient(new MockHandler([ $response ]));

        return new RdStationAdapter($client, $this->config);
    }

    protected function createMockClient(MockHandler $mock)
    {
        $history = Middleware::history($this->container);
        $stack = tap(HandlerStack::create($mock))->push($history);

        return new Client([ 'http_errors' => false, 'handler' => $stack ]);
    }
}

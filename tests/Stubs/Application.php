<?php

namespace Otimizy\RdStation\Tests\Stubs;

class Application
{
    protected $bounds = [];

    public function singleton($abstract, callable $concrete)
    {
        $this->bounds[] = [ $abstract, $concrete([ 'config' => [
            'services.rdstation' => null,
        ] ]) ];
    }

    public function getBounds()
    {
        return $this->bounds;
    }
}

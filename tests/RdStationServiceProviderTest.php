<?php

namespace Otimizy\RdStation\Tests;

use PHPUnit\Framework\TestCase;
use Otimizy\RdStation\RdStationAdapter;
use Otimizy\RdStation\Tests\Stubs\Application;
use Otimizy\RdStation\RdStationServiceProvider;

class RdStationServiceProviderTest extends TestCase
{
    protected $provider;
    protected $application;

    public function setUp(): void
    {
        parent::setUp();

        $this->application = new Application;
        $this->provider = new RdStationServiceProvider($this->application);
    }

    public function testRegister()
    {
        $response = $this->provider->register();

        $this->assertNull($response);
        $this->assertCount(1, $this->application->getBounds());

        list($abstract, $concrete) = $this->application->getBounds()[0];

        $this->assertEquals('rdstation', $abstract);
        $this->assertInstanceOf(RdStationAdapter::class, $concrete);
    }

    public function testProvides()
    {
        $response = $this->provider->provides();

        $this->assertEquals([ 'rdstation' ], $response);
    }
}

<?php

namespace Otimizy\RdStation\Tests;

use Orchestra\Testbench\TestCase;
use Otimizy\RdStation\Facades\RdStation;
use Otimizy\RdStation\RdStationServiceProvider;

class RdStationFacadeTest extends TestCase
{
    public function testAuthUrl()
    {
        $url = RdStation::authUrl();

        $exptectedUrl = 'https://api.rd.services/auth/dialog'
            . '?client_id=123456789&redirect_url='
            . urlencode('https://mysite.com/callback');

        $this->assertEquals($exptectedUrl, $url);
    }

    protected function getPackageProviders($app)
    {
        return [ RdStationServiceProvider::class ];
    }

    protected function getPackageAliases($app)
    {
        return [ 'RdStation' => RdStation::class ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('services.rdstation', [
            'client_id' => '123456789',
            'client_secret' => '987654321',
            'redirect_url' => 'https://mysite.com/callback',
        ]);
    }
}

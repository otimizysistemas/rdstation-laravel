[![pipeline status](https://gitlab.com/otimizysistemas/rdstation-laravel/badges/master/pipeline.svg)](https://gitlab.com/otimizysistemas/rdstation-laravel/commits/master)
[![coverage report](https://gitlab.com/otimizysistemas/rdstation-laravel/badges/master/coverage.svg)](https://gitlab.com/otimizysistemas/rdstation-laravel/commits/master)

# RD Station - Laravel

Este é um pacote para realizar a integração de uma aplicação Laravel com a API 2.0 do RD Station.

## Instalação

Instale o pacote via Composer.

```shell
composer require otimizysistemas/rdstation-laravel
```

Caso você não utilize o auto-discovery, registre o provider e o alias em sua aplicação.

```php
Otimizy\RdStation\RdStationServiceProvider::class,

...

'RdStation' => Otimizy\RdStation\Facades\RdStation::class,
```

Abra o arquivo `config\services.php` e adicione as configurações do seu app da RD Station.

```php
[
    ...
    'rdstation' => [
        'client_id' => '...',
        'client_secret' => '...',
        'redirect_url' => '...',
    ],
]
```
## Métodos disponíveis

Os seguintes métodos estão disponíveis para uso na API.

* `RdStation::authUrl()`: Gera a URL de redirecionamento, necessária para obter a autorização do usuário;
* `RdStation::getToken($code)`: Com o retorno da URL de redirecionamento, use o código recebido para obter `access_token` e `refresh_token`, bem como a data de expiração;
* `RdStation::refreshToken($refreshToken)`: Obtém novos tokens e nova data de expiração;
* `RdStation::createLead($identifier, $email, $payload, $token)`: Cria um novo Lead no RD Station;
* `RdStation::setOpportunity($email, $funnel, $token)`: Marca um Lead no RD Station como "oportunidade";
* `RdStation::setWon($email, $funnel, $token)`: Marca um Lead no RD Station como "oportunidade ganha".

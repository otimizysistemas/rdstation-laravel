<?php

namespace Otimizy\RdStation;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class RdStationServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton('rdstation', function ($app) {
            $client = new Client([
                'base_uri' => 'https://api.rd.services/',
                'http_errors' => false,
            ]);

            $config = $app['config'];

            return new RdStationAdapter($client, $config['services.rdstation']);
        });
    }

    public function provides()
    {
        return [ 'rdstation' ];
    }
}

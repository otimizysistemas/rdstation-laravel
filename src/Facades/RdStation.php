<?php

namespace Otimizy\RdStation\Facades;

use Illuminate\Support\Facades\Facade;

class RdStation extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'rdstation';
    }
}

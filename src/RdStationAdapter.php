<?php

namespace Otimizy\RdStation;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class RdStationAdapter
{
    protected $client;
    protected $config;

    public function __construct(Client $client, $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    public function authUrl()
    {
        return 'https://api.rd.services/auth/dialog?client_id='
            . $this->config['client_id'] . '&redirect_url='
            . urlencode($this->config['redirect_url']);
    }

    public function getToken($code)
    {
        return $this->sendAuth([ 'code' => $code ]);
    }

    public function refreshToken($refreshToken)
    {
        return $this->sendAuth([ 'refresh_token' => $refreshToken ]);
    }

    public function createLead($identifier, $email, $payload, $token)
    {
        return $this->sendEvent('CONVERSION', array_merge($payload, [
            'conversion_identifier' => $identifier,
            'email' => $email,
        ]), $token);
    }

    public function setOpportunity($email, $funnel, $token)
    {
        return $this->sendEvent('OPPORTUNITY', [
            'funnel_name' => $funnel,
            'email' => $email,
        ], $token);
    }

    public function setWon($email, $funnel, $token)
    {
        return $this->sendEvent('SALE', [
            'funnel_name' => $funnel,
            'email' => $email,
        ], $token);
    }

    protected function sendAuth($body)
    {
        $response = $this->makePost('auth/token', array_merge([
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
        ], $body));

        return $this->parseBody($response);
    }

    protected function sendEvent($type, $payload, $token)
    {
        $response = $this->makePost('platform/events', [
            'event_type' => $type,
            'event_family' => 'CDP',
            'payload' => $payload,
        ], $token);

        return $this->parseBody($response);
    }

    protected function makePost($url, $body, $token = null)
    {
        $options = [ 'json' => $body ];

        if ($token !== null) {
            $options['headers'] = [ 'Authorization' => 'Bearer ' . $token ];
        }

        return $this->client->post($url, $options);
    }

    protected function parseBody(ResponseInterface $response)
    {
        return [
            'status' => $response->getStatusCode(),
            'body' => json_decode((string) $response->getBody(), true),
        ];
    }
}
